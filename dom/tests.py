# -*- coding=utf-8 -*-
__author__ = 'Joanna667'

import unittest
import microblog
from webtest import TestApp
from bottle import post,route,run,template,get,view,request,redirect
import bottle_sqlite


class ServerTest(unittest.TestCase):
    def setUp(self):
        self.app = TestApp(microblog.app)
        self.app.get("/")

    def badLoginTest(self):
        response = self.app.post('/login', {'login': 'ktos', 'password': 'ktos'})
        assert response.status_int == 200
        response.mustcontain('niepoprawne dane')

    def loginTest(self):
        response = self.app.post('/login', {'login': 'Joanna', 'password': 'Joanna667'})
        assert response.status_int == 200
        response.mustcontain('Zaloguj się')
    #sprawdzanie funkcjonalnosci dla zalogowanego uzytkownika
    def showPostsTest(self):
        response = self.app.get('/blog')
        assert response.status_int == 200
        response.mustcontain('Moje wpisy')

    def addPostTest(self):
        response = self.app.post('/dodaj', {'content': 'wiadomość testowa - test poszedł pomyślnie'})
        assert response.status_int == 200

    def addEmptyPostTest(self):
        response = self.app.post('/dodaj', {'content': ''})
        assert response.status_int == 200
        response.mustcontain('Wpis nie może byc pusty!')

    def logOutTest(self):
        response = self.app.get('/logout')
        assert response.status_int == 200

    #a co jak nie jestes zalogowany?
    def showPosts_u(self):
        response = self.app.get('/blog')
        assert response.status_int == 200
        response.mustcontain('Aby przeglądać bloga, musisz się zalogować!')

    def addPostTest_u(self):
        response = self.app.post('/dodaj', {'content': 'wiadomość testowa - test poszedł pomyślnie'})
        assert response.status_int == 200
        response.mustcontain('Aby dodawać wpisy trzeba byc zalogowanym!')

    #def testBadRequest(self):
        #response = self.app.get('/costam', status=404)
        #assert response.status_int == 404

if __name__ == '__main__':
    unittest.main()
