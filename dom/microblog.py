# -*- coding=utf-8 -*-
__author__ = 'Joanna667'
import bottle
from bottle import route, run, template, install, post, request, redirect, default_app
from bottle_sqlite import SQLitePlugin
import sys
from sesja import Sesja

plugin = SQLitePlugin(dbfile='baza.db')
install(plugin)


sess = Sesja()
sess.set('user', '')


@route('/')
def wpisy(db):
    #wykonywane tylko raz
    #db.execute('CREATE TABLE Users(id integer primary key, login text, password text)')
    #db.execute('CREATE TABLE Posty(id integer primary key, autor text, tresc text, data date)')
    #db.execute("INSERT INTO Users (login, password) VALUES ('Joanna', 'Joanna667')")
    #db.execute("INSERT INTO Users (login, password) VALUES ('Gość', 'qwerty11')")
    #wykonywane zawsze
    dane = db.execute("select autor, tresc, data from Posty")
    lista = dane.fetchall()
    return template("index.tpl", posty=lista)


@route('/login')
def login(db):
    return template("login.tpl", kom='')


@post('/login')
def login(db):
    name = request.forms.get('login')
    password = request.forms.get('password')
    if name == '' or password == '':
        return template('login.tpl', kom="niepoprawne dane")
    users = db.execute("SELECT id, login, password From Users")

    for user in users:
        if user[1] == name and user[2] == password:
            sess.set('user', name)
            return redirect("/~p12/wsgi/blog")
    return template('login.tpl', kom='nie ma takiego użytkownika')


@route('/blog')
def blog(db):
    if sess.read('user') != '':
        db.execute("")
        dane = db.execute("select autor, tresc, data from Posty where autor=='%s'" % sess.read('user'))
        lista = dane.fetchall()
        return template("blog.tpl", posty=lista)
    else:
        return template('login.tpl', kom='Aby przeglądać bloga, musisz się zalogować!')


@route('/dodaj')
def dodaj():
    if sess.read('user') != '':
        return template("dodaj.tpl", kom='')
    else:
        return template('login.tpl', kom='Aby dodawać wpisy trzeba byc zalogowanym!')


@post('/dodaj')
def dodaj(db):
    content = request.forms.get('content')
    user = sess.read('user')
    if content == '':
        return template('dodaj.tpl', kom="Wpis nie może byc pusty!")
    else:
        command = "Insert Into Posty (tresc, autor, data) Values ('%s', '%s', date())" % (content, user)
        db.execute(command)
    return redirect("/~p12/wsgi/blog")


@route('/logout')
def logout():
    sess.set('user', '')
    return redirect('/~p12/wsgi/')

if __name__ == "__main__":
    run(host='194.29.175.240', port=16000, debug=True)

app = bottle.default_app()